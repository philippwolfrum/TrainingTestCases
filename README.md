[![experimental](http://badges.github.io/stability-badges/dist/experimental.svg)](http://github.com/badges/stability-badges)
# DEXPI Interoperability Matrix
Automatically created by DEXPI-Matrix-Crawler version 0.3
Tue Jul 17 19:28:00 CEST 2018
##  Interoperability Matrix: 
__ |      AUD|      AVV|      HEX|      SAG|      VTT|      XVT|      
---       |---      |---      |---      |---      |---      |---      
AUD |     0% |      0% |      0% |      1,8% |    14,3% |   1,8% |    
AVV |     7,1% |    0% |      0% |      5,4% |    14,3% |   3,6% |    
HEX |     19,6% |   0% |      0% |      10,7% |   17,9% |   5,4% |    
SAG |     21,4% |   1,8% |    0% |    3,6% |    17,9% |   1,8% |    
VTT |     0% |      0% |      0% |      0% |      0% |      0% |      
XVT |     0% |      0% |      0% |      0% |      0% |      0% |    

##  Preliminary statistical information: 
###  Analyzed Test Cases: 
Number of TestCases analysed:56
* C01V01
* C02V01
* C03V01
* C04V01
* C05V01
* C06V01
* E01V01
* E01V02
* E02V01
* E02V02
* E03V01
* E03V02
* E04V01
* E04V02
* E05V01
* E05V02
* E06V01
* E06V02
* E07V01
* E08V01
* E09V01
* E11V01
* E12V01
* E13V01
* I01V01
* I01V02
* I02V01
* I02V02
* I03V01
* I03V02
* I04V01
* I04V02
* I05V01
* I05V02
* I06V01
* I06V02
* I07V01
* I07V02
* I08V01
* I08V02
* I09V01
* I09V02
* I10V01
* I10V02
* I11V01
* I11V02
* I12V01
* I12V02
* P01V01
* P01V02
* P02V01
* P02V02
* P03V01
* P03V02
* P04V01
* P04V02
###  Analyzed relevant Test Cases: 
* Found: AUD -> SAG (1) in E01V01-AUD.EX01-SAG.IM01
* Found: AUD -> VTT (1) in E01V01-AUD.EX01-VTT.IM01
* Duplicate: AUD -> VTT (1) in E01V01-AUD.EX01-VTT.IM02
* Duplicate: AUD -> VTT (1) in E01V01-AUD.EX02-VTT.IM01
* Found: AUD -> VTT (2) in E03V01-AUD.EX01-VTT.IM01
* Found: AUD -> VTT (3) in E04V01-AUD.EX01-VTT.IM01
* Found: AUD -> VTT (4) in E05V01-AUD.EX01-VTT.IM01
* Found: AUD -> VTT (5) in E06V01-AUD.EX01-VTT.IM01
* Found: AUD -> VTT (6) in P01V01-AUD.EX01-VTT.IM01
* Found: AUD -> VTT (7) in P03V01-AUD.EX01-VTT.IM01
* Found: AUD -> VTT (8) in P04V01-AUD.EX01-VTT.IM01
* Found: AUD -> XVT (1) in E01V01-AUD.EX01-XVT.IM01
* Duplicate: AUD -> XVT (1) in E01V01-AUD.EX02-XVT.IM01
* Duplicate: AUD -> XVT (1) in E01V01-AUD.EX03-XVT.IM01
* Found: AVV -> AUD (1) in C02V01-AVV.EX01-AUD.IM01
* Found: AVV -> AUD (2) in C03V01-AVV.EX01-AUD.IM01
* Found: AVV -> AUD (3) in I01V01-AVV.EX01-AUD.IM01
* Found: AVV -> AUD (4) in I03V01-AVV.EX01-AUD.IM01
* Found: AVV -> SAG (1) in C02V01-AVV.EX01-SAG.IM01
* Found: AVV -> SAG (2) in C03V01-AVV.EX01-SAG.IM01
* Found: AVV -> SAG (3) in C04V01-AVV.EX01-SAG.IM01
* Found: AVV -> VTT (1) in C02V01-AVV.EX01-VTT.IM01
* Found: AVV -> VTT (2) in C03V01-AVV.EX01-VTT.IM01
* Found: AVV -> VTT (3) in E01V01-AVV.EX01-VTT.IM01
* Duplicate: AVV -> VTT (3) in E01V01-AVV.EX01-VTT.IM02
* Duplicate: AVV -> VTT (3) in E01V01-AVV.EX02-VTT.IM01
* Found: AVV -> VTT (4) in E02V01-AVV.EX01-VTT.IM01
* Found: AVV -> VTT (5) in I01V01-AVV.EX01-VTT.IM01
* Found: AVV -> VTT (6) in I02V01-AVV.EX01-VTT.IM01
* Found: AVV -> VTT (7) in P01V01-AVV.EX01-VTT.IM01
* Found: AVV -> VTT (8) in P02V01-AVV.EX01-VTT.IM01
* Found: AVV -> XVT (1) in E01V01-AVV.EX01-XVT.IM01
* Found: AVV -> XVT (2) in E02V01-AVV.EX01-XVT.IM01
* Found: HEX -> AUD (1) in C01V01-HEX.EX03-AUD.IM01
* Found: HEX -> AUD (2) in C02V01-HEX.EX01-AUD.IM01
* Found: HEX -> AUD (3) in C03V01-HEX.EX01-AUD.IM01
* Found: HEX -> AUD (4) in E02V01-HEX.EX02-AUD.IM01-VTT.IM01
* Duplicate: HEX -> AUD (4) in E02V01-HEX.EX02-AUD.IM01
* Found: HEX -> AUD (5) in I01V02-HEX.EX01-AUD.IM01
* Duplicate: HEX -> AUD (5) in I01V02-HEX.EX03-AUD.IM01
* Duplicate: HEX -> AUD (5) in I01V02.HEX.EX03-AUD.IM01
* Found: HEX -> AUD (6) in I02V01-HEX.EX03-AUD.IM01
* Duplicate: HEX -> AUD (6) in I02V01-HEX.EX01-AUD.IM01
* Duplicate: HEX -> AUD (6) in I02V01-HEX.EX02-AUD.IM01
* Found: HEX -> AUD (7) in I03V01-HEX.EX03-AUD.IM01
* Found: HEX -> AUD (8) in I04V01-HEX.EX03-AUD.IM01
* Duplicate: HEX -> AUD (8) in I04V01-HEX.EX02-AUD.IM01
* Found: HEX -> AUD (9) in I09V02-HEX.EX03-AUD.IM01
* Found: HEX -> AUD (10) in I11V01-HEX.EX03-AUD.IM01
* Duplicate: HEX -> AUD (10) in I11V01-HEX.EX01-AUD.IM01
* Found: HEX -> AUD (11) in P01V01-HEX.EX02-AUD.IM01
* Found: HEX -> SAG (1) in C01V01-HEX.EX02-SAG.IM01
* Duplicate: HEX -> SAG (1) in C01V01-HEX.EX02-SAG.IM02
* Found: HEX -> SAG (2) in C02V01-HEX.EX01-SAG.IM01
* Found: HEX -> SAG (3) in C03V01-HEX.EX01-SAG.IM01
* Found: HEX -> SAG (4) in C04V01-HEX.EX01-SAG.IM01
* Found: HEX -> SAG (5) in E01V01-HEX.EX01-SAG.IM01
* Found: HEX -> SAG (6) in P01V01-HEX.EX02-SAG.IM01
* Found: HEX -> VTT (1) in C01V01-HEX.EX02-VTT.IM01
* Found: HEX -> VTT (2) in C02V01-HEX.EX01-VTT.IM01
* Found: HEX -> VTT (3) in C03V01-HEX.EX01-VTT.IM01
* Found: HEX -> VTT (4) in C04V01-HEX.EX01-VTT.IM01
* Found: HEX -> VTT (5) in E01V01_HEX.EX02-VTT.IM01
* Duplicate: HEX -> VTT (5) in E01V01_HEX.EX03-VTT.IM01
* Found: HEX -> VTT (6) in E02V01-HEX.EX02-AUD.IM01-VTT.IM01
* Duplicate: HEX -> VTT (6) in E02V01-HEX.EX02-VTT.IM01
* Duplicate: HEX -> VTT (6) in E02V01-HEX.EX03-VTT.IM01
* Found: HEX -> VTT (7) in I01V01-HEX.EX01-VTT.IM01
* Found: HEX -> VTT (8) in I02V01-HEX.EX01-VTT.IM01
* Found: HEX -> VTT (9) in P01V01-HEX.EX02-VTT.IM01
* Found: HEX -> VTT (10) in P02V01-HEX.EX01-VTT.IM01
* Found: HEX -> XVT (1) in E01V01-HEX.EX01-XVT.IM01
* Found: HEX -> XVT (2) in E02V01-HEX.EX01-XVT.IM01
* Found: HEX -> XVT (3) in I01V01-HEX.EX01-XVT.IM01
* Found: SAG -> AUD (1) in I01V01-SAG.EX01-AUD.IM01
* Duplicate: SAG -> AUD (1) in I01V01-SAG.EX02-AUD.IM01
* Duplicate: SAG -> AUD (1) in I01V01-SAG.EX03-AUD.IM01
* Found: SAG -> AUD (2) in I02V01-SAG.EX01-AUD.IM01
* Found: SAG -> AUD (3) in I03V01-SAG.EX01-AUD.IM01
* Found: SAG -> AUD (4) in I04V01-SAG.EX01-AUD.IM01
* Found: SAG -> AUD (5) in I05V01-SAG.EX01-AUD.IM01
* Found: SAG -> AUD (6) in I06V01-SAG.EX01-AUD.IM01
* Found: SAG -> AUD (7) in I07V01-SAG.EX01-AUD.IM01
* Found: SAG -> AUD (8) in I08V01-SAG.EX01-AUD.IM01
* Found: SAG -> AUD (9) in I10V01-SAG.EX01-AUD.IM01
* Found: SAG -> AUD (10) in I11V01-SAG.EX01-AUD.IM01
* Found: SAG -> AUD (11) in P01V01-SAG.EX02-AUD.IM01
* Found: SAG -> AUD (12) in P02V01-SAG.EX02-AUD.IM01
* Found: SAG -> AVV (1) in E02V01-SAG.EX01-AVV.IM01
* Found: SAG -> HEX (1) in P02V01_SAG.EX01-HEX.IM01 --> TWEAKED! 
* Found: SAG -> SAG (1) in C04V01-SAG.EX01-SAG.IM01
* Found: SAG -> SAG (2) in C05V01-SAG.EX01-SAG.IM01
* Found: SAG -> VTT (1) in C03V01-SAG.EX01-VTT.IM01
* Found: SAG -> VTT (2) in E01V01_SAG.EX01-VTT.IM01
* Found: SAG -> VTT (3) in E02V01-SAG.EX01-VTT.IM01
* Duplicate: SAG -> VTT (3) in E02V01-SAG.EX02-VTT.IM01
* Found: SAG -> VTT (4) in I01V02-SAG.EX02-VTT.IM02
* Duplicate: SAG -> VTT (4) in I01V02_SAG.EX02-VTT.IM01
* Duplicate: SAG -> VTT (4) in I01V02_SAG.EX03-VTT.IM01
* Found: SAG -> VTT (5) in I02V01-SAG.EX01-VTT.IM01
* Found: SAG -> VTT (6) in I03V01-SAG.EX01-VTT.IM01
* Found: SAG -> VTT (7) in I09V01-SAG.EX01-VTT.IM01
* Found: SAG -> VTT (8) in I10V01-SAG.EX01-VTT.IM01
* Found: SAG -> VTT (9) in P01V01-SAG.EX01-VTT.IM01
* Found: SAG -> VTT (10) in P02V01_SAG.EX01-VTT.IM01
* Found: SAG -> XVT (1) in E01V01-SAG.EX01-XVT.IM01
